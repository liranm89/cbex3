import java.util.ArrayList;
import java.util.HashMap;
/**
 * Tamar Wasserman			Liran Matatof
 * ID: 305179202			201015724
 */

/**
 * Main class. It initializes all the parameters, reads from the file and runs the algorithm 
 */
public class Main {
	private static final String FILE_NAME = "Digits.txt";
	private static final String RESULTS = "results.csv";
	public static final int FIRST_NOTATION = 0, SECOND_NOTATION = 1;
	public static final int ROWS = 10, COLS = 10, size = ROWS*COLS;
	public static int SAMPLES_PER_DIGIT = 1, TOTAL_DIGITS = 10;
	public static double ERROR_RATE = 0.1;
	public static final boolean minusOneNotation = false;
	private HashMap<Integer, ArrayList<String>> samples;
	private int[][] matrix;
	private int[] digitsOfTrained;
	private FileWriterAndReader file = new FileWriterAndReader();
	public static void main(String[] args) {
		//Initialize a variable
		Main hopfieldMain = new Main();
		hopfieldMain.readSamples();
		hopfieldMain.runWithAllParameters();
	}
	private void runWithAllParameters() {
		String csvString = "";
		
		/*
		 * Digits of trained example:
		 * if we have 3 samples per digit, and we have digits 0 to 2 it's going to look like this:
		 * 0, 0, 0, 1, 1, 1, 2, 2, 2
		 */
		//	for (ERROR_RATE = 0; ERROR_RATE < 1; ERROR_RATE = ERROR_RATE + 0.01) { //for part B graph.
		//For each digit
		for (int i = 1; i <= TOTAL_DIGITS; i++) {
			//initialize a matrix
			this.matrix = new int[i * SAMPLES_PER_DIGIT][Main.size];
			/*
			 * Digits of trained example:
			 * if we have 3 samples per digit, and we have digits 0 to 2 it's going to look like this:
			 * 0, 0, 0, 1, 1, 1, 2, 2, 2
			 */
			this.digitsOfTrained = new int[i * SAMPLES_PER_DIGIT];
			//fill from zero to i
			fillMatrixFromFirstTo(i);
			//initialize algorithm
			Algorithm algorithm = new Algorithm(size, matrix, digitsOfTrained, minusOneNotation,
					FIRST_NOTATION, SECOND_NOTATION, SAMPLES_PER_DIGIT, ROWS, COLS, ERROR_RATE);
			//start the algorithm
			csvString += algorithm.start();
		}
		//	}
		//	this.file.writeToFile(RESULTS, csvString);

	}
	
	/**
	 * @param max_digit The maximum digit, from 1 to maximum included
	 */
	private void fillMatrixFromFirstTo(int max_digit) {
		//For each digit
		for (int i = 0, m = 0; i < max_digit; i++) {
			//Get its samples
			ArrayList<String> currentNumberSamples = samples.get(i);
			//For each sample
			for (int j = 0; j < SAMPLES_PER_DIGIT; j++) {
				//put it inside the matrix
				char[] currentNumber = currentNumberSamples.get(j).toCharArray();
				for (int k = 0; k < matrix[j].length; k++) {
					matrix[m][k] = Character.getNumericValue(currentNumber[k]);
				}
				this.digitsOfTrained[m] = i;
				m++;
			}
		}
	}
	
	/**
	 * read the samples from the file
	 */
	private void readSamples() {
		//Read from file
		try {
			samples = this.file.readFromFile(FILE_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
