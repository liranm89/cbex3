import java.util.Comparator;

/**
 * This class sorts Integers vector
 */
public class IntegerSorter implements Comparator<Integer> {
	public int compare(Integer arg0, Integer arg1) {
		return arg0 > arg1 ? 1 : arg0 == arg1 ? 0 : -1;
	}

}
