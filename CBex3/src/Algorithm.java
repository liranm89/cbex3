import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
/**
 * This class implements the Hopfield Network Algorithm.
 */
public class Algorithm {
	//all parameters and variables
	private int[][] matrix;
	private int[][] trainingSet;
	private int[] digitsOfTrained;
	private int cols, rows, firstNotation, secondNotation, samplesPerDigit;
	private double errorRate;
	private boolean minusOneNotation;
	private int wrongDigitThatExists = 0, wrongDigitThatDoesNotExist = 0;
	private String csvData;
	/**
	 * Algorithm constructor.
	 * 
	 * @param matrixSize the size of the T matrix
	 * @param trainingSet the training set
	 * @param digitsOfTrained the digit of each trained digit by its order
	 * @param minusOneNotation whether we use 0/1 notation or -1/1 notation
	 * @param firstNotation first notation, specific for the targil 0
	 * @param secondNotation second notation, specific for the targil 1
	 * @param samplesPerDigit samples per digit
	 * @param rows rows of the matrix
	 * @param cols cols of the matrix
	 * @param errorRate error rate for each sample
	 */
	public Algorithm(int matrixSize, int[][] trainingSet, int[] digitsOfTrained, boolean minusOneNotation, int firstNotation, int secondNotation, int samplesPerDigit, int rows, int cols, double errorRate) {
		setMatrix(new int[matrixSize][matrixSize]);
		setTrainingSet(trainingSet);
		setDigitsOfTrained(digitsOfTrained);
		this.minusOneNotation = minusOneNotation;
		this.cols = cols;
		this.rows = rows;
		this.firstNotation = firstNotation;
		this.secondNotation = secondNotation;
		this.samplesPerDigit = samplesPerDigit;
		this.errorRate = errorRate;
	}
	
	/**
	 * @return trainingSet
	 */
	public int[][] getTrainingSet() {
		return trainingSet;
	}

	/**
	 * @param trainingSet the training set
	 */
	public void setTrainingSet(int[][] trainingSet) {
		this.trainingSet = trainingSet;
	}
	
	/**
	 * @return T matrix
	 */
	public int[][] getMatrix() {
		return matrix;
	}
	
	/**
	 * 
	 * @param matrix T matrix (weights matrix)
	 */
	public void setMatrix(int[][] matrix) {
		this.matrix = matrix;
	}
	
	/**
	 * This method learns the examples, puts them into T matrix
	 */
	public void learn() {
		int i, j;
		//for each col
		for (i = 0; i < matrix.length - 1; i++) {
			int sum = 0;
			//for each second col
			for (j = i + 1; j < matrix[i].length; j++) {
				//for each sample
				for (int k = 0; k < this.trainingSet.length; k++) {
					//sum if cols are the same
					if (this.trainingSet[k][i] == this.trainingSet[k][j]) {
						sum++;
					} else {
						sum--;
					}
				}
				//put it into the matrix
				this.matrix[i][j] = sum;
				this.matrix[j][i] = sum;
				sum = 0;
			}
		}
	}

	/**
	 * this function prints the T matrix
	 */
	public void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/**
	 * This function distorts a digit
	 * 
	 * @param goodDigit digit before distortion
	 * @return digit after distortion
	 */
	public int[] distort(int[] goodDigit) {
		int[] badDigit = Arrays.copyOf(goodDigit, goodDigit.length);
		int changeBits = (int)(badDigit.length * this.errorRate);
		//Get random numbers
		ArrayList<Integer> randomIndexes = chooseSubset(badDigit.length, 0, changeBits);
		randomIndexes.sort(new IntegerSorter());
		//For each bit
		for (int i = 0, k = 0; i < badDigit.length && k < changeBits; i++) {
			//if it's a chosen bit then bit flip
			if (i == randomIndexes.get(k)) {
				if (badDigit[i] == this.firstNotation) {
					badDigit[i] = this.secondNotation;
				} else {
					badDigit[i] = this.firstNotation;
				}
				k++;
			}
		}
		return badDigit;

	}

	/**
	 * This method reconstructs the digit
	 * @param digit the digit which we want to reconstruct
	 * @return
	 */
	public int[] reconstruct(int[] digit) {
		//for each bit
		for (int i = 0; i < digit.length; i++) {
			int sum = 0;
			//for other bits
			for (int j = 0; j < digit.length; j++) {
				if (i == j) {
					continue;
				}
				//sum with the matrix
				if (digit[j] == 0 && minusOneNotation == true) {
					sum += -1 * matrix[i][j];
				} else {
					sum += digit[j] * matrix[i][j];
				}
			}
			//Change
			if (sum >= 0) {
				digit[i] = this.secondNotation;
			} else {
				digit[i] = this.firstNotation;
			}
		}
		return digit;
	}


	/**
	 * This function chooses a subset of numbers.
	 * 
	 * @param limit The maximum number (excluded)
	 * @param minimum The minimum number (included)
	 * @param subsetSize The size of the subset
	 * @return An Integer ArrayList of the subset
	 */
	public static ArrayList<Integer> chooseSubset(int limit, int minimum, int subsetSize) {
		//Initialize a random variable
		Random rand = new Random();
		//Subset of indexes to scramble
		ArrayList<Integer> subsetToScramble = new ArrayList<Integer>();
		//We need a list of numbers from 0 to limit - 1, so we can choose from them randomly, efficiently. 
		int listOfNumbers[] = new int[limit];
		//Fill listOfNumbers
		for (int i = 0; i < limit; i++) {
			listOfNumbers[i] = i;
		}
		//Do this the subset size times - means every time a random index will be inserted to subsetToScramble
		for (int i = 0; i < subsetSize; i++) {
			//Get a random index
			int randomIndex = rand.nextInt(limit - minimum) + minimum;
			//Add the number at the random index
			subsetToScramble.add(listOfNumbers[randomIndex]);
			//We don't want to encounter this number again, therefore we switch
			//Between the number in the random index to the last index
			listOfNumbers[randomIndex] = listOfNumbers[limit - 1];
			//Now we will reduce limit to limit - 1.
			limit--;
		}
		return subsetToScramble;
	}
	
	/**
	 * This method prints a digit
	 * 
	 * @param digit a digit we want to print to the screen
	 */
	public void printDigit(int[] digit) {
		//For each row
		for (int i = 0; i < this.rows; i++) {
			//for each col
			for (int j = 0; j < this.cols; j++) {
				//print the pixel/bit
				System.out.print(digit[(int) (i* this.cols + j)] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	/**
	 * This is the main method of the algorithm.
	 * It learns, distorts each sample N times, tries to reconstruct, writing all results, etc.
	 * 
	 * @return A string representing number of digits, success rate, and distortion rate
	 */
	public String start() {
		this.learn();
		double[] successRate = new double[this.trainingSet.length / this.samplesPerDigit];
		int distortedSamples = 1000;
		System.out.println("Trained with digits: 0 to " + this.digitsOfTrained[this.digitsOfTrained.length - 1]);
		System.out.println();
		//For each digit
		for (int i = 0; i < this.trainingSet.length / this.samplesPerDigit; i++) {
			int[][] arrayOfDistortedDigits = new int[distortedSamples][100];
			//Distort N times
			for (int j = 0; j < distortedSamples; j++) {
				//pick a random sample from digit i
				int index = new Random().nextInt(this.samplesPerDigit) + i * this.samplesPerDigit;
				arrayOfDistortedDigits[j] = this.distort(this.trainingSet[index]);
			}
			boolean failureExample = true;
			//For each distorted sample, reconstruct
			for (int k = 0; k < arrayOfDistortedDigits.length; k++) {
				int[] old = new int[arrayOfDistortedDigits[k].length];
				//Construct until convergence
				while (!arraysEqual(old, arrayOfDistortedDigits[k])) {
					old = Arrays.copyOf(arrayOfDistortedDigits[k], arrayOfDistortedDigits[k].length);
					this.reconstruct(arrayOfDistortedDigits[k]);
				}
				if (gotToRightNumber(arrayOfDistortedDigits[k], this.digitsOfTrained[i * this.samplesPerDigit])) {
					successRate[i]++;
				} else {
					if (failureExample) {
			//			System.out.println("Example for a failure to reconstruct digit:\t" + this.digitsOfTrained[i * this.samplesPerDigit]);
			//			this.printDigit(arrayOfDistortedDigits[k]);
						failureExample = false;
					}
				}
			}
		}
		showFinalResults(successRate, distortedSamples);
		return this.csvData;
	}
	
	/**
	 * This method shows the final results to the screen
	 * 
	 * @param successRate an array containing success rate for each digit
	 * @param distortedSamples number of distorted samples per digit
	 */
	private void showFinalResults(double[] successRate, int distortedSamples) {
		//Just showing the results. Nothing special
		String sep = "\t";
		System.out.print("[" + sep + 0 + sep);
		for (int i = this.samplesPerDigit; i < this.digitsOfTrained.length; i = i + this.samplesPerDigit) {
			System.out.print("|" + sep + this.digitsOfTrained[i] + sep);
		}
		System.out.println("]");
		System.out.print("[" + sep + 100 * successRate[0]/distortedSamples + sep);
		for (int i = 1; i < successRate.length; i++) {
			System.out.print("|" + sep + 100 * successRate[i]/distortedSamples + sep);
		}
		System.out.println("]");
		double totalSuccess = 0;
		for (int i = 0; i < successRate.length; i++) {
			totalSuccess += successRate[i]/distortedSamples;
		}
		totalSuccess = (totalSuccess / successRate.length) * 100;
		System.out.println("Total success: " + totalSuccess + "\n");
		this.csvData = successRate.length + "," + totalSuccess + "," + this.errorRate*100 + "\n";
		int totalWrongs = this.wrongDigitThatDoesNotExist + this.wrongDigitThatExists;
		System.out.println("Error #1: Wrong digit that was taught:\t" + 100 * (double)this.wrongDigitThatExists / totalWrongs + "%");
		System.out.println("Error #2: Wrong digit that was not taught:\t" + 100 * (double)this.wrongDigitThatDoesNotExist / totalWrongs + "%");
	}
	
	/**
	 * This function checks of the reconstructed digit has got to the right number or not
	 * 
	 * @param arrayOfDistortedDigits the digit
	 * @param digit the digit it should be
	 * @return true or false
	 */
	private boolean gotToRightNumber(int[] arrayOfDistortedDigits, int digit) {
		boolean wrongDigit = false;
		//For each sample in the training set
		for (int i = 0; i < this.trainingSet.length; i++) {
			//if it equals
			if (arraysEqual(arrayOfDistortedDigits, this.trainingSet[i])) {
				//check if it's the correct digit
				if (this.digitsOfTrained[i] == digit) {
					return true;
				} else {
					wrongDigit = true;
					break;
				}
			}
		}
		//Count between the two errors
		if (wrongDigit) {
			this.wrongDigitThatExists++;
		} else this.wrongDigitThatDoesNotExist++;
		return false;

	}
	
	/**
	 * This method checks of two arrays equal
	 * 
	 * @param a1 array1
	 * @param a2 array2
	 * @return true or false
	 */
	private boolean arraysEqual(int[] a1, int[] a2) {
		if (a1.length != a2.length) {
			return false;
		}
		for (int i = 0; i < a1.length; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @return digits of the trained set
	 */
	public int[] getDigitsOfTrained() {
		return digitsOfTrained;
	}

	/**
	 * @param digitsOfTrained digits of the trained set
	 */
	public void setDigitsOfTrained(int[] digitsOfTrained) {
		this.digitsOfTrained = digitsOfTrained;
	}
}
