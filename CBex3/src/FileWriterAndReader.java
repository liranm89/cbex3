

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * This class handles files - reading from a file and writing a file
 */
public class FileWriterAndReader {
	private PrintWriter pw;
	private static final int CHANGE_NUMBER = 10;
	/**
	 * Writing to a file a given string
	 * @param fileName destination file
	 * @param stringToWrite input string
	 */
	public void writeToFile(String fileName, String stringToWrite) {
		try {
			this.pw = new PrintWriter(new File(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		this.pw.write(stringToWrite);
		this.pw.close();
	}

	/**
	 * This method reads a file
	 * @param fileName the name of the file
	 * @return the data from the file
	 * @throws Exception if there was a problem with the file
	 */
	public HashMap<Integer, ArrayList<String>> readFromFile(String fileName) throws Exception {
		FileInputStream	fileInputStream = new FileInputStream(new File(fileName));
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
		String line = null;
		String input = "";
		ArrayList<String> listOfSamples = new ArrayList<String>();
		HashMap<Integer, ArrayList<String>> mapFromDigitToArrays = new HashMap<Integer, ArrayList<String>>();
		int count = 0, sampleNumber = 0;
		//read each line
		while ((line = bufferedReader.readLine()) != null) {
			//If it's an empty line
			if (!line.contains(String.valueOf(Main.FIRST_NOTATION)) && 
					!line.contains(String.valueOf(Main.SECOND_NOTATION))) {
				//Add it to the list of samples for this digit
				listOfSamples.add(input);
				count++;
				input = "";
				//Change digit
				if (count == CHANGE_NUMBER) {
					//Put it inside a hashMap
					mapFromDigitToArrays.put(sampleNumber, listOfSamples);
					listOfSamples = new ArrayList<String>();
					sampleNumber++;
					count = 0;
				}
			} else {
				input += line;
			}
		}
		bufferedReader.close();
		return mapFromDigitToArrays;
	}
}
